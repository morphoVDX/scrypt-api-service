```javascript
// Add this to your .env
  REACT_APP_API_SERVICE_BASE_URL=https://sae.vizidox.com:8443/
  REACT_APP_AUTH_BASE_URL=https://staging.vizidox.com/auth/realms/sae/protocol/openid-connect/

// Add this to your App constructor
  import { auth, api, configApiService } from 'sae-api-service';

  constructor(props) {
    super(props);

    const authorizationToken = auth.get();

    configApiService(api.instance, {
      authorizationToken,
      baseURL: process.env.REACT_APP_API_SERVICE_BASE_URL,
      authBaseURL: process.env.REACT_APP_AUTH_BASE_URL,
      httpErrorHandler: auth.handleHttpError({
        loginRedirectUri: '/dashboard',
      }),
    });

    this.state = {
      hasAuthorizationToken: Boolean(authorizationToken),
    };
  }
```