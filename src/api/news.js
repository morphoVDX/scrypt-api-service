import instance from './instance';

export const getById = async (newsId, params) => {
  const response = await instance.get(`/news/${newsId}`, { params });

  return response.data;
};

export const get = async (params) => {
  const response = await instance.get('/news', {
    params,
  });

  return response;
};
