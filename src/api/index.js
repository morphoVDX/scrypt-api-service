/**
 * All other exported modules share this base instance.
 * This means that if you want to add a default header
 * to all requests, you should add it to this base instance.
 */
// App Interceptors
import instance from './instance';
import defaultResponseInterceptors from './responseInterceptors';
// App Services
import * as users from './users';
import * as news from './news';
import * as bookings from './bookings';

defaultResponseInterceptors.forEach((interceptor) => {
  instance.interceptors.response.use(interceptor);
});

export {
  instance,
  users,
  news,
  bookings,
  defaultResponseInterceptors,
};
