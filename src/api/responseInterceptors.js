import forIn from 'lodash/forIn';
import { parseUrl } from 'query-string';

export default [
  (response) => {
    const { _embedded = {}, _links, page } = response.data || response;
    const listName = Object.keys(_embedded).find((name) => name.includes('BeanList'));
    const pages = {};


    if (_links && page) {
      forIn(_links, (value, key) => {
        pages[key] = 'parseUrl(value.href).query';
      });

      return {
        list: _embedded[listName] || [],
        pagination: {
          pages,
          ...page,
        }
      };
    }

    return response;
  }
];
