// import qs from 'qs';
import instance from './instance';

export const getById = async (bookingId, params) => {
  const response = await instance.get(`/booking/${bookingId}`, { params });

  const booking = response.data;

  const itemList = booking.itemList.map((item) => ({ ...item, type: 'itemList' }));
  const roomList = booking.roomList.map((item) => ({ ...item, type: 'roomList' }));

  booking.itemList = [...itemList, ...roomList];

  return response;
};

// TODO: This request should be filled with user ID into params also.
export const get = async (params) => {
  const response = await instance.get('/booking', { params });
  const { list } = response;

  list.forEach((booking) => {
    const itemList = booking.itemList.map((item) => ({ ...item, type: 'itemList' }));
    const roomList = booking.roomList.map((item) => ({ ...item, type: 'roomList' }));

    booking.itemList = [...itemList, ...roomList];
  });

  return response;
};

export const patch = async (id, params) => {
  const config = { headers: { 'Content-Type': 'application/json' } };
  const response = await instance.patch(`/booking/${id}`, params, config);

  return response.data;
};

export const post = async (payload) => {
  const config = { headers: { 'Content-Type': 'application/json' } };
  const response = await instance.post('/booking', { ...payload }, config);

  return response.data;
};

export const cancel = async (id, params) => {
  const response = await instance.post(`/booking/${id}/cancel`, params);

  return response.data;
};

