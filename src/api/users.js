import instance from './instance';

// Today authenticatication is just returning
// the user according to Authorization header.
export const authenticate = async (params) => {
  const response = await instance.get('/user', { params });

  return response.data;
};

export const logout = async (params) => {
  const response = await instance.get('/user/logout', { params });

  return response.data;
};

export const patch = async (id, params) => {
  const config = { headers: { 'Content-Type': 'application/json' } };
  const response = await instance.patch('/user', params, config);

  return response.data;
};

export const updateAvatar = async (userId, file) => {
  const formData = new FormData();

  formData.append('file', new Blob([file], { type: file.type }), 'file');

  const response = await instance.post(`/user/${userId}/avatar`, formData);

  return response;
};
