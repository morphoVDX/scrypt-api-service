import axios from 'axios';
import { stringify } from 'query-string';

const instance = axios.create();

instance.defaults.paramsSerializer = (query) => stringify(query);

export default instance;
