import * as apiFolder from './api';
import * as authFolder from './auth';

export { default as configApiService } from './configApiService';

export const api = apiFolder;
export const auth = authFolder;
