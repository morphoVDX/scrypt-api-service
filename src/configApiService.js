export default (instance, {
  baseURL,
  authBaseURL,
  authorizationToken,
  httpErrorHandler,
}) => {
  instance.defaults.baseURL = baseURL;
  instance.defaults.maxRedirects = 0;
  instance.defaults.headers.common.Accept = 'application/json';
  instance.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

  if (authorizationToken) {
    instance.defaults.headers.common.Authorization = authorizationToken;
  }

  if (httpErrorHandler) {
    instance.interceptors.response.use(
      (response) => response,
      (error) => httpErrorHandler(error, { authBaseURL: authBaseURL || baseURL }),
    );
  }
};

