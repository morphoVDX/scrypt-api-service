import { stringify } from 'querystring';
import urlJoin from 'url-join';

const mountQueryString = (redirectURL = window.location.origin) => stringify({
  client_id: 'sae-app',
  redirect_uri: redirectURL,
});

export default (authBaseURL, redirectURL) => urlJoin(
  authBaseURL,
  'logout',
  `?${mountQueryString(redirectURL)}`
);
