import Cookies from 'js-cookie';
import mountLogoutPath from './mountLogoutPath';
import { parse } from 'querystring';

const ACCESS_TOKEN_PATHNAME = 'access_token';

export { default as handleHttpError } from './handleHttpError';

export { default as mountLoginPath } from './mountLoginPath';

export const remove = () => Cookies.remove(ACCESS_TOKEN_PATHNAME);

export const get = () => {
    const { expires_in, access_token, token_type } = parse(window.location.hash);

    if (access_token) {
      const expires = new Date(new Date().getTime() + (expires_in * 1000));
      const token = `${token_type} ${access_token}`;

      persist(token, expires);
      window.history.replaceState(null, null, ' ');
    }

    return Cookies.get(ACCESS_TOKEN_PATHNAME);
}

export const persist = (token, expires) => Cookies.set(ACCESS_TOKEN_PATHNAME, token, { expires });

export const logout = ({ authBaseURL, redirectURL }) => {
    remove();
    window.location.assign(mountLogoutPath(authBaseURL, redirectURL));
}
