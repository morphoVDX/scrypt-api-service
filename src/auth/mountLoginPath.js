import { stringify } from 'querystring';
import nonceFast from 'nonce-fast';
import urlJoin from 'url-join';

const nonce = nonceFast(5); // the length of the nonce

const mountQueryString = (redirectUri) => stringify({
  response_type: 'token',
  client_id: 'sae-app',
  redirect_uri: urlJoin(window.location.origin, redirectUri),
  scope: 'openid',
  nonce: nonce(),
});

export default (authBaseUrl, redirectUri) => urlJoin(
  authBaseUrl,
  'auth',
  `?${mountQueryString(redirectUri)}`
);
