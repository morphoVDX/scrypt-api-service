import { get } from 'lodash';
import * as auth from '../auth';

const handleUnauthorizedError = (error, { authBaseUrl, loginRedirectUri }) => {
  auth.remove();
  window.location.assign(auth.mountLoginPath(authBaseUrl, loginRedirectUri));
};

const handleBadRequest = (error) => {
  const { global, ...fields } = get(error, 'response.data.errors', {});

  error.global = global;
  error.fields = fields;

  return Promise.reject(error);
};

export default (config = {}) => (error, apiServiceConfig = {}) => {
  const { status } = error.response;

  const reject = Promise.reject.bind(Promise);

  const errorHandler = ({
    401: handleUnauthorizedError,
    400: handleBadRequest
  }[status]) || reject;

  return errorHandler(error, { ...config, ...apiServiceConfig });
};
